import React, { Component } from 'react';
import * as firebase from 'firebase';
import AppButton from "../AppButton";
import {  View } from 'react-native';
import { Card, Text, Image } from "react-native-elements";
import RestaurantRating from "./RestaurantRating";
import PreLoader from "../../components/PreLoader";
import Toast from 'react-native-simple-toast';

export default class Restaurant extends Component {
	constructor() {
		super();
		this.state = {
			imageFirebase: "",
			loaded: false,
		}
	}
	
	
	loadImage = async () => {
		const { id } = this.props.restaurant;
		firebase
			.storage()
			.ref(`Campañas/${id}`)
			.getDownloadURL()
			.then(resolve => {
				this.setState({
					imageFirebase: resolve,
					loaded: true
				});
			})
			.catch(error => {
				Toast.showWithGravity('Error '+error, Toast.LONG, Toast.BOTTOM);

			});
	};
	componentDidMount() {
		this.loadImage();
	}

	render() {
		const { imageFirebase, loaded } = this.state;
		const { editRestaurant, goHome, restaurant ,deleteCampaña} = this.props;
		if (!loaded) {
			return <PreLoader />
		} else {
			return (

				<Card
					title={restaurant.name}
				>
					<View style={{
						justifyContent: 'center',
						alignItems: 'center',
						marginBottom:10,
					}} >
						{imageFirebase &&
							<Image source={{ uri: imageFirebase }} style={{ width: 200, height: 200 }} />}
					</View>
					<RestaurantRating restaurantId={restaurant.id} />

					<Text style={{ marginBottom: 10, marginTop: 10 }}>
						{restaurant.description}
					</Text>

					<AppButton
						bgColor="#00B8D4"
						title="Editar Campaña"
						action={editRestaurant}
						iconName="pencil"
						iconSize={30}
						iconColor="#fff"
					/>
					<AppButton
						bgColor="#D50000"
						title="Eliminar Campaña"
						action={deleteCampaña}
						iconName="remove"
						iconSize={30}
						iconColor="#fff"
					/>
					<AppButton
						bgColor="#80CBC4"
						title="Volver"
						action={goHome}
						iconName="arrow-left"
						iconSize={30}
						iconColor="#fff"
					/>

				</Card>
			)
		}
	}
}