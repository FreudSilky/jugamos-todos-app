import React, {Component} from 'react';
import AppButton from "../AppButton";
import {options, Comment} from '../../forms/comment';
import t from 'tcomb-form-native';
const Form = t.form.Form;
import {Card} from "react-native-elements";
import {View} from "react-native";
import * as firebase from 'firebase';
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import { dataAction } from '../../../actions';
class CommentForm extends Component {
	constructor() {
		super();
		this.state = {
			comment: {
				comment: '',
				rating: 1
			}
		};
	}

	addComment () {
		const validate = this.refs.form.getValue();
		let comment = Object.assign({}, validate);
		if(comment.comment) {
			let data = {};
			let ref = firebase.database().ref().child('comments');
			const key = ref.push().key;
			let data2={
				comment: comment.comment,
				rating: comment.rating,
				email:store.getState().UserData.data.email,
			}
			data[`${this.props.restaurantId}/${key}`] = data2;

			ref.update(data).then(() => {
				this.setState((prevState, props) => {
					return {
						comment: {
							comment: '',
							rating: 1,
						}
					}
				});
				Toast.showWithGravity('Comentario publicado!', Toast.LONG, Toast.TOP);
			})
		}
	}

	onChange (comment) {
		this.setState({comment});
	}

	render () {
		const {comment} = this.state;
		return (
			<Card title="Dános tu opinión">
				<View>
					<Form
						ref="form"
						type={Comment}
						options={options}
						value={comment}
						onChange={(v) => this.onChange(v)}
					/>
				</View>

				<AppButton
					bgColor="#00B8D4"
					title="Publicar opinión"
					action={this.addComment.bind(this)}
					iconName="comments"
					iconSize={30}
					iconColor="#fff"
				/>
			</Card>
		)
	}
}
const mapStateToProps = state => ({
	dataUser: state.dataUser
});


const mapDispatchToProps = dispatch => ({
	dataAction: data => dispatch(dataAction(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CommentForm);