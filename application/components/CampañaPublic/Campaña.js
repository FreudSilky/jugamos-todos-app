import React, { Component } from 'react';
import * as firebase from 'firebase';
import AppButton from "../AppButton";
import {  View } from 'react-native';
import { Card, Text, Image } from "react-native-elements";
import RestaurantRating from "./../Restaurant/RestaurantRating";
import PreLoader from "../PreLoader";

export default class Campaña extends Component {
	constructor() {
		super();
		this.state = {
			imageFirebase: "",
			loaded: false,
		}
	}
	loadImage = async () => {
		const { id } = this.props.restaurant;
		console.log(id);
		firebase
			.storage()
			.ref(`Campañas/${id}`)
			.getDownloadURL()
			.then(resolve => {
				console.log(resolve);
				this.setState({
					imageFirebase: resolve,
					loaded: true
				});
			})
			.catch(error => {
				console.log(error);
			});
	};
	componentDidMount() {
		this.loadImage();
	}

	render() {
		const { imageFirebase, loaded } = this.state;
		const {  goHome, restaurant } = this.props;
		if (!loaded) {
			return <PreLoader />
		} else {
			return (

				<Card
					title={restaurant.name}
				>
					<View style={{
						justifyContent: 'center',
						alignItems: 'center',
						marginBottom:10,
					}} >
						{imageFirebase &&
							<Image source={{ uri: imageFirebase }} style={{ width: 200, height: 200 }} />}
					</View>
					<RestaurantRating restaurantId={restaurant.id} />

					<Text style={{ marginBottom: 10, marginTop: 10 }}>
						{restaurant.description}
					</Text>
					<AppButton
						bgColor="#80CBC4"
						title="Volver"
						action={goHome}
						iconName="arrow-left"
						iconSize={30}
						iconColor="#fff"
					/>

				</Card>
			)
		}
	}
}