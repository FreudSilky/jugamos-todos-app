import React, {Component} from 'react';
import * as firebase from 'firebase';
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import {dataAction} from '../../actions';
class Logout extends Component {
	componentDidMount () {
		firebase.auth().signOut()
			.then(() => {
				Toast.showWithGravity("Has cerrado sesión correctamente", Toast.LONG, Toast.BOTTOM);
				this.props.dataAction(null);
			})
			.catch(error => {
				Toast.showWithGravity(error.message, Toast.LONG, Toast.BOTTOM);
			})
	}

	render () {
		return null;
	}
}
const mapStateToProps = state => ({
	UserData:state.UserData
 });
 
 const mapDispatchToProps = dispatch => ({
	dataAction: data => dispatch(dataAction(data)),
  });
 
export default connect(mapStateToProps, mapDispatchToProps)(Logout);