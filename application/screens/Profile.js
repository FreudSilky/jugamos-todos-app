import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';
import BackgroundImage from "../components/BackgroundImage";
import { Card, Input, Avatar } from "react-native-elements";
import AppButton from "../components/AppButton";
import * as firebase from 'firebase';
import Toast from 'react-native-simple-toast';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';
import { connect } from 'react-redux';
import { dataAction } from '../../actions';
class Profile extends Component {
	constructor() {
		super();
		this.state = {
			email: "",
			imageFirebase: "",
			user: {
				name: '',
				age: ''
			},

		}
	}
	uploadImage = uri => {
		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();
			xhr.onerror = reject;
			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					resolve(xhr.response);
				}
			};

			xhr.open("GET", uri);
			xhr.responseType = "blob";
			xhr.send();
		});
	};

	openGallery = async () => {
		const resultPermission = await Permissions.askAsync(
			Permissions.CAMERA_ROLL
		);

		if (resultPermission) {
			const resultImagePicker = await ImagePicker.launchImageLibraryAsync({
				allowsEditing: true,
				aspect: [4, 3]
			});

			if (resultImagePicker.cancelled === false) {
				const imageUri = resultImagePicker.uri;
				const { id } = store.getState().UserData;
				this.uploadImage(imageUri)
					.then(resolve => {
						let ref = firebase
							.storage()
							.ref()
							.child(`images/${id}`);
						ref
							.put(resolve)
							.then(resolve => {
								this.loadImage();
								Toast.showWithGravity('Imagen subida correctamente', Toast.LONG, Toast.BOTTOM);
							})
							.catch(error => {
								Toast.showWithGravity('Error al subir la imagen ', Toast.LONG, Toast.BOTTOM);
							});
					})
					.catch(error => {
						Toast.showWithGravity("Error al subir la imagen", Toast.LONG, Toast.BOTTOM);
					});
			}
		}
	};

	loadImage = async () => {
		const { id } = store.getState().UserData;
		firebase
			.storage()
			.ref(`images/${id}`)
			.getDownloadURL()
			.then(resolve => {
				this.setState({
					imageFirebase: resolve
				});
			})
			.catch(error => {
				Toast.showWithGravity("Agrega una foto", Toast.LONG, Toast.BOTTOM);

			});
	};

	checkImage = () => {
		const { imageFirebase } = this.state;

		if (imageFirebase) {
			return (
				<Avatar
					size="xlarge"
					rounded
					title="Ph"
					source={{ uri: imageFirebase }}
					onPress={() => this.openGallery()}
					activeOpacity={0.7}
				/>
			);
		} else {
			return (
				<Avatar
					size="xlarge"
					rounded
					title="Ph"
					onPress={() => this.openGallery()}
					activeOpacity={0.7}
				/>
			);

		}
		return null;
	};

	async checarDataUser() {
		await firebase.auth().onAuthStateChanged((user) => {
			if (user !== null) {
				this.props.dataAction({ id: user.uid, data: user.providerData[0], isLogued: true });
				this.setState({
					email: store.getState().UserData.data.email,
				});
				this.loadImage();
			} else {
			}
		})
	}
	componentDidMount() {
		this.checarDataUser();
		this.fetch().then(() => {
			Toast.showWithGravity('Usuario obtenido', Toast.LONG, Toast.BOTTOM);
		})
	}

	updateName(val) {
		let state = this.state.user;
		this.setState({
			user: Object.assign({}, state, {
				name: val
			})
		});
	}

	updateAge(val) {
		let state = this.state.user;
		this.setState({
			user: Object.assign({}, state, {
				age: val
			})
		});
	}

	render() {
		const { user } = this.state;
		return (
			<BackgroundImage source={require('../../assets/images/Perfil.png')}>
				<Card title='Datos Personales'
					 >
					<View style={{
						justifyContent: 'center',
						alignItems: 'center',
					}} >
						{this.checkImage()}

					</View>
					<Input
						placeholder="Correo"
						shake={true}
						value={this.state.email}
						onChangeText={(val) => this.updateName(val)}
					/>
					<Input
						placeholder="Nombre del usuario"
						shake={true}
						value={user.name}
						onChangeText={(val) => this.updateName(val)}
					/>
					<Input
						placeholder="Edad del usuario"
						shake={true}
						value={user.age}
						onChangeText={(val) => this.updateAge(val)}
					/>
					<View style={{ marginTop: 12 }}>
						<AppButton
							bgColor="#00B8D4"
							title="Guardar en local"
							action={this.save.bind(this)}
							iconName="save"
							iconSize={30}
							iconColor="#fff"
						/>
					</View>
				</Card>
			</BackgroundImage>
		);
	}

	async save() {
		try {
			const user = {
				name: this.state.user.name,
				age: this.state.user.age
			};
			await AsyncStorage.setItem('user', JSON.stringify(user));
			Toast.showWithGravity('Usuario guardado correctamente', Toast.LONG, Toast.BOTTOM);
		} catch (error) {
			Toast.showWithGravity('Error guardando', Toast.LONG, Toast.BOTTOM);
		}
	}

	async fetch() {
		try {
			let user = await AsyncStorage.getItem('user');
			if (user) {
				let parsed = JSON.parse(user);
				this.setState({ user: parsed });
			}
		} catch (error) {
			Toast.showWithGravity('Error obteniendo', Toast.LONG, Toast.BOTTOM);
		}
	}
}
const mapStateToProps = state => ({
	dataUser: state.dataUser
});


const mapDispatchToProps = dispatch => ({
	dataAction: data => dispatch(dataAction(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Profile);