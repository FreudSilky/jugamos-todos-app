import React, { Component } from 'react';
import BackgroundImage from "../../components/BackgroundImage";
import AppButton from "../../components/AppButton";
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import * as firebase from 'firebase';
import { options, Restaurant } from '../../forms/restaurant';
import t from 'tcomb-form-native';
import { Card } from "react-native-elements";
const Form = t.form.Form;
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import * as Action from '../../../actions';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
class AddCampañas extends Component {
	constructor() {
		super();
		this.state = {
			restaurant: {
				name: '',
				address: '',
				capacity: 0,
				description: ''
			},
			image: null,

		};
	}
	componentDidMount() {
	}
	uploadImage = uri => {
		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();
			xhr.onerror = reject;
			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					resolve(xhr.response);
				}
			};

			xhr.open("GET", uri);
			xhr.responseType = "blob";
			xhr.send();
		});
	};
	_pickImage = async () => {
		const resultPermission = await Permissions.askAsync(
			Permissions.CAMERA_ROLL
		);
		if (resultPermission) {
			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.All,
				allowsEditing: true,
				aspect: [4, 3],
				quality: 1
			});
			if (!result.cancelled) {
				this.setState({ image: result.uri });
			}
		}else{
			alert('Sorry, we need camera roll permissions to make this work!');
		}
	};
	save()  {
		const validate = this.refs.form.getValue();
		const {image} =this.state
		if (validate && image) {
			const { id } = store.getState().UserData;
			const { name, address, capacity, description} = this.state.restaurant;
			const data2 = {
				id, name, address, capacity, description
			}
			console.log(this.state);
			let data = {};
			const key = firebase.database().ref().child('Campañas').push().key;
			data[`Campañas/${key}`] = data2;
			firebase.database().ref().update(data).then(() => {
				Toast.showWithGravity('Campañas dada de alta', Toast.LONG, Toast.BOTTOM);
				this.props.navigation.navigate('ListRestaurants');
			});
			this.uploadImage(image)
					.then(resolve => {
						let ref = firebase
							.storage()
							.ref()
							.child(`Campañas/${key}`);
						ref
							.put(resolve)
							.then(resolve => {
								Toast.showWithGravity('Imagen subida correctamente ', Toast.LONG, Toast.BOTTOM);
							})
							.catch(error => {
								Toast.showWithGravity('Error al subir la imagen ', Toast.LONG, Toast.BOTTOM);

							});
					})
					.catch(error => {
						Toast.showWithGravity('Error ', Toast.LONG, Toast.BOTTOM);
					});
		}else{
			Toast.showWithGravity('LLene todos lo campos', Toast.LONG, Toast.BOTTOM);
		}
	}

	onChange(restaurant) {
		this.setState({ restaurant });
	}

	render() {
		const { restaurant } = this.state;
		let { image } = this.state;
		return (

			<BackgroundImage source={require('../../../assets/images/Agregar.jpg')}>
				<ScrollView>
					<View style={styles.container}>
						<Card title="Formulario de Campañas">
							<View style={{
								justifyContent: 'center',
								alignItems: 'center',
							}} >
								{image &&
									<Image source={{ uri: image }} style={{ width: 200, height: 200,marginBottom:10 }} />}
								
								<AppButton
								bgColor="#00B8D4"
								title="Cargar Imagen"
								action={this._pickImage}
								iconName="file-image-o"
								iconSize={30}
								iconColor="#fff"
							/>
							</View>
							<View>
								<Form
									ref="form"
									type={Restaurant}
									options={options}
									value={restaurant}
									onChange={(v) => this.onChange(v)}
								/>
							</View>
							<AppButton
								bgColor="#00B8D4"
								title="Dar de alta"
								action={this.save.bind(this)}
								iconName="plus"
								iconSize={30}
								iconColor="#fff"
							/>
						</Card>
					</View>
				</ScrollView>
			</BackgroundImage>
		)
	}
}

const mapStateToProps = state => ({
	dataUser: state.dataUser,
	ImageUri: state.ImageUri,
});


export default connect(mapStateToProps, Action)(AddCampañas);

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'rgba(231, 228, 224, 0.8)',
		padding: 10
	}
});