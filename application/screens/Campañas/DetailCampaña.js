import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { ScrollView } from "react-native";
import * as firebase from 'firebase';
import BackgroundImage from "../../components/BackgroundImage";
import Restaurant from "../../components/Restaurant/Restaurant";
import CommentForm from "../../components/Comment/CommentForm";
import CommentList from "../../components/Comment/CommentList";
import Toast from 'react-native-simple-toast';

export default class Campaña extends Component {
	constructor(props) {
		super(props);
		const { params } = props.navigation.state;
		this.state = {
			restaurant: params.restaurant
		};
	}
	editRestaurant() {
		const navigateAction = NavigationActions.navigate({
			routeName: 'EditRestaurant',
			params: { restaurant: this.state.restaurant }
		});
		this.props.navigation.dispatch(navigateAction);
	}
	deleteCampaña() {
		const { id } = this.state.restaurant;
		if (id) {
			const ref = firebase.database().ref().child(`Campañas/${id}`);
			ref.remove().
				then(() => {
					Toast.showWithGravity('Eliminado Correctamente', Toast.LONG, Toast.BOTTOM);
					const navigateAction = NavigationActions.navigate({
						routeName: 'ListRestaurants',
					});
					this.props.navigation.dispatch(navigateAction);
				}).catch(function (error) {
					Toast.showWithGravity('Error', Toast.LONG, Toast.BOTTOM);
				});
		}
	}
	goHome() {
		const navigateAction = NavigationActions.navigate({
			routeName: 'ListRestaurants',
		});
		this.props.navigation.dispatch(navigateAction);
	}


	render() {
		const { restaurant } = this.state;
		return (
			<BackgroundImage source={require('../../../assets/images/Editar.jpg')}>
				<ScrollView>

					<Restaurant
						goHome={this.goHome.bind(this)}
						editRestaurant={this.editRestaurant.bind(this)}
						deleteCampaña={this.deleteCampaña.bind(this)}
						restaurant={restaurant}
					/>

					<CommentForm restaurantId={restaurant.id} />

					<CommentList restaurantId={restaurant.id} />
				</ScrollView>
			</BackgroundImage>
		)
	}
}