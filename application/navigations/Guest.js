import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import StartScreen from '../screens/Start';
import LoginScreen from '../screens/Login';
import RegisterScreen from '../screens/Register';
const AppNavigator = createStackNavigator(
	{
		start: StartScreen,
		login:LoginScreen,
		register: RegisterScreen,
	  },
	  {
		initialRouteName: 'start',
		headerLayoutPreset: 'center',
		defaultNavigationOptions: {
			headerStyle: {
                backgroundColor: '#009688',
                elevation: 0,
                shadowOpacity: 0
            },
            headerTintColor: '#333333',
            headerTitleStyle: {
                fontWeight: 'bold',
                color: '#ffffff'
            }
		},
	  }
);

export default createAppContainer(AppNavigator);