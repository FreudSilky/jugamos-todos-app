import React, { Component } from 'react';
import Logout from '../screens/Logout';
import { createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Font from 'react-native-vector-icons/FontAwesome';
import CampañaScreen from "../screens/Campañas/Campaña";
import AddCampañasScreen from "../screens/Campañas/AddCampañas";
import DetailScreen from "../screens/Campañas/DetailCampaña";
import EditCampañatScreen from "../screens/Campañas/EditCampaña";
import ProfileScreen from "../screens/Profile";
import CampañasPublic from '../screens/CampañasPublic/Campaña';
import CampañasPublicDetail from '../screens/CampañasPublic/DetailCampaña';
const navigationOption={
		headerLayoutPreset: 'center',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: '#009688',
				elevation: 0,
				shadowOpacity: 0
			},
			headerTintColor: '#333333',
			headerTitleStyle: {
				fontWeight: 'bold',
				color: '#ffffff'
			}
		},
};
const campañasScreenStackPublic = createStackNavigator(
	{
		ListCampañaPublic: {
			screen: CampañasPublic,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Campañas',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()}color="white" name="navicon" size={30} />
				  )
				};
			  }
		},
		DetailCampañaPublic: {
			screen: CampañasPublicDetail,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Detalle dela Campaña',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="navicon" size={30} />
				  )
				};
			  }
		},
	},
	navigationOption
);

const restaurantsScreenStack = createStackNavigator(
	{
		ListRestaurants: {
			screen: CampañaScreen,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Campañas',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()}color="white" name="navicon" size={30} />
				  )
				};
			  }
		},
		AddRestaurant: {
			screen: AddCampañasScreen,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Añadir Campaña',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="navicon" size={30} />
				  )
				};
			  }
		},
		EditRestaurant: {
			screen: EditCampañatScreen,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Editar Campaña',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="navicon" size={30} />
				  )
				};
			  }
		},
		DetailRestaurant: {
			screen: DetailScreen,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Detalle dela Campaña',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="navicon" size={30} />
				  )
				};
			  }
		},
	},
	navigationOption
);
const profileScreenStack = createStackNavigator(
	{
		ProfileScreen: {
			screen: ProfileScreen,
			navigationOptions: ({ navigation }) => {
				return {
				  headerTitle: 'Perfil',
				  headerLeft: (
					<Font style={{ paddingLeft: 10 }} color="white"  onPress={() => navigation.openDrawer()} name="navicon" size={30} />
				  )
				};
			  }
		}
	},
	navigationOption
	
);



const AppDrawerNavigator = createDrawerNavigator(
  
  {
	'Mis Campañas':{
		screen:restaurantsScreenStack,
		navigationOptions: ({ navigation }) => {
		  return {
			drawerIcon: (
			  <Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="tree" size={15} />
			)
		  };
		}
	},
	Campañas:{
		screen:campañasScreenStackPublic,
		navigationOptions: ({ navigation }) => {
		  return {
			drawerIcon: (
			  <Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="tree" size={15} />
			)
		  };
		}
	},
	Perfil: {
	  screen: profileScreenStack,
	  navigationOptions: ({ navigation }) => {
		  return {
			drawerIcon: (
			  <Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white"  name="user-circle-o" size={15} />
			)
		  };
		}
  },
	  Salir: {
		screen: Logout,
		navigationOptions: ({ navigation }) => {
		  return {
			drawerIcon: (
			  <Font style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color="white" name="sign-out" size={15} />
			)
		  };
		}
	  },
	  
	
  },
  {
	initialRouteName: 'Perfil',
	drawerBackgroundColor:"#009688",
	drawerType:'slide',
		drawerPosition:"right",
		
		contentOptions: {
			activeTintColor: 'white',
			inactiveTintColor:'white',
			activeBackgroundColor:'#80CBC4' ,
			itemsContainerStyle: {
			  marginVertical: 0,
			},
			iconContainerStyle: {
			  opacity: 89
			}
		  },
  }
);

export default createAppContainer(AppDrawerNavigator);
