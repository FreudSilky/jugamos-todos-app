import { combineReducers } from 'redux';
import Data from './Data'
import UserData from './UserData'
import ImageUri from './ImageUri'
export default combineReducers({
  Data,UserData:UserData,ImageUri:ImageUri
});
