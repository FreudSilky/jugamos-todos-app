import { INCREMENT,DECREMENT,DATAUSER,IMAGENURI } from './actionTypes';

export const IncremetAction = () => ({
  type: INCREMENT
});
export const decrementtAction = () => ({
  type: DECREMENT
});
export const dataAction = data => ({
  type: DATAUSER,
  payload:data
});
export const imageUriAction = uri => ({
  type: IMAGENURI,
  payload:uri
});