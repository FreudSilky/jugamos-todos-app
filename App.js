import React from 'react';
import firebaseConfig from './application/utils/Firebase';
import * as firebase from 'firebase';
import { Provider } from 'react-redux';
import Guest from './application/navigations/Guest';
import PreLoader from './application/components/PreLoader';
import Logged from './application/navigations/Logged';
console.disableYellowBox = true;
console.ignoredYellowBox = ['Setting a timer'];
firebase.initializeApp(firebaseConfig);
import store from './store';
export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isLogged: false,
      loaded: false
    }
  }


  async componentDidMount() {
    await firebase.auth().onAuthStateChanged((user) => {
      if (user !== null) {
        this.setState({
          isLogged: true,
          loaded: true
        });
      } else {
        this.setState({
          isLogged: false,
          loaded: true
        });
      }
    })
  }

  render() {
    const {isLogged, loaded} = this.state;

		if ( ! loaded) {
			return (<Provider store={store}><PreLoader></PreLoader></Provider>);
		}
		if(isLogged) {
			return (<Provider store={store}>
        <Logged></Logged>
        </Provider>);
		} else {
			return (<Provider store={store}><Guest /></Provider>);
		}
  }
}




